package ru.t1.dkozyaikin.tm.service;

import ru.t1.dkozyaikin.tm.api.repository.IProjectRepository;
import ru.t1.dkozyaikin.tm.api.service.IProjectService;
import ru.t1.dkozyaikin.tm.enumerated.Status;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkozyaikin.tm.exception.field.*;
import ru.t1.dkozyaikin.tm.model.Project;

public final class ProjectService extends AbstractService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String name) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.add(new Project(name));
    }

    @Override
    public Project create(final String name, final String description) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.add(new Project(name, description));
    }

    @Override
    public Project create(final String name, final String description, final Status status) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (status == null) return null;
        return repository.add(new Project(name, description, status));
    }

    @Override
    public Project updateById(final String id, final String name, final String description) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null || status.getDisplayName().isEmpty()) throw new StatusEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        if (status == null || status.getDisplayName().isEmpty()) throw new StatusEmptyException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}

