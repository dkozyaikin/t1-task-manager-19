package ru.t1.dkozyaikin.tm.service;

import ru.t1.dkozyaikin.tm.api.repository.IProjectRepository;
import ru.t1.dkozyaikin.tm.api.repository.ITaskRepository;
import ru.t1.dkozyaikin.tm.api.service.IProjectTaskService;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkozyaikin.tm.exception.entity.TaskNotFoundException;
import ru.t1.dkozyaikin.tm.exception.field.IndexIncorrectException;
import ru.t1.dkozyaikin.tm.exception.field.ProjectIdEmptyException;
import ru.t1.dkozyaikin.tm.exception.field.TaskIdEmptyException;
import ru.t1.dkozyaikin.tm.model.Project;
import ru.t1.dkozyaikin.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(
            final IProjectRepository projectRepository,
            final ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String projectId, final String taskId) throws AbstractException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

    @Override
    public void removeProjectById(final String projectId) throws AbstractException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final List<Task> tasks = taskRepository.findAllTasksByProjectId(projectId);
        for (final Task task : tasks) taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);
    }

    @Override
    public void removeProjectByIndex(Integer projectIndex) throws AbstractException {
        if (projectIndex == null || projectIndex < 0 ) throw new IndexIncorrectException();
        if (projectIndex >= projectRepository.getSize()) throw new IndexIncorrectException();
        final Project project = projectRepository.findOneByIndex(projectIndex);
        if (project == null) throw new ProjectNotFoundException();
        final List<Task> tasks = taskRepository.findAll();
        for (final Task task : tasks) {
            if (project.getId().equals(task.getProjectId())) taskRepository.removeById(task.getId());
        }
        projectRepository.removeByIndex(projectIndex);
    }


    @Override
    public void clearAllProjects() {
        final List<Project> projects = projectRepository.findAll();
        final List<Task> tasks = taskRepository.findAll();
        for (final Project project: projects) {
            for (final Task task : tasks) {
                if (project.getId().equals(task.getProjectId())) taskRepository.removeById(task.getId());
            }
        }
        projectRepository.clear();
    }

    @Override
    public void unbindTaskFromProject(final String projectId, final String taskId) throws AbstractException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

}
