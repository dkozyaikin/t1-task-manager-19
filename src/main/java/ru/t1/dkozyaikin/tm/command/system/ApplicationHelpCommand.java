package ru.t1.dkozyaikin.tm.command.system;

import ru.t1.dkozyaikin.tm.api.model.ICommand;
import ru.t1.dkozyaikin.tm.command.AbstractCommand;
import ru.t1.dkozyaikin.tm.exception.AbstractException;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    public static final String NAME = "help";

    public static final String ARGUMENT = "-h";

    public static final String DESCRIPTION = "Show list of terminal commands";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) System.out.println(command);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
