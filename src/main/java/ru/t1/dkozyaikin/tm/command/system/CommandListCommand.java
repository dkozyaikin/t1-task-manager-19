package ru.t1.dkozyaikin.tm.command.system;

import ru.t1.dkozyaikin.tm.api.model.ICommand;
import ru.t1.dkozyaikin.tm.command.AbstractCommand;
import ru.t1.dkozyaikin.tm.exception.AbstractException;

import java.util.Collection;

public final class CommandListCommand extends AbstractSystemCommand {

    public static final String NAME = "command";

    public static final String ARGUMENT = "-cmd";

    public static final String DESCRIPTION = "Show command list";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
