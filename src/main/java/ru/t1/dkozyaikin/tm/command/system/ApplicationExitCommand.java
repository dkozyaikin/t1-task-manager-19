package ru.t1.dkozyaikin.tm.command.system;

import ru.t1.dkozyaikin.tm.exception.AbstractException;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    public static final String NAME = "exit";

    public static final String DESCRIPTION = "exit from application";

    @Override
    public void execute() throws AbstractException {
        System.exit(0);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
