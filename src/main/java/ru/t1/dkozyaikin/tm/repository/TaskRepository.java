package ru.t1.dkozyaikin.tm.repository;

import ru.t1.dkozyaikin.tm.api.repository.ITaskRepository;
import ru.t1.dkozyaikin.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public Task create(String name) {
        return add(new Task(name));
    }

    @Override
    public Task create(String name, String description) {
        return add(new Task(name, description));
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String projectId) {
        final List<Task> tasksWithCurrentProjectId = new ArrayList<>();
        for (Task task : models) {
            if (projectId.equals(task.getProjectId())) tasksWithCurrentProjectId.add(task);
        }
        return tasksWithCurrentProjectId;
    }

}
