package ru.t1.dkozyaikin.tm.repository;

import ru.t1.dkozyaikin.tm.api.repository.IProjectRepository;
import ru.t1.dkozyaikin.tm.enumerated.Status;
import ru.t1.dkozyaikin.tm.model.Project;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project create(String name) {
        return add(new Project(name));
    }

    @Override
    public Project create(String name, String description) {
        return add(new Project(name, description));
    }

    @Override
    public Project create(String name, String description, Status status) {
        return add(new Project(name, description, status));
    }

}
