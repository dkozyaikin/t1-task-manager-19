package ru.t1.dkozyaikin.tm.exception.user;

import ru.t1.dkozyaikin.tm.exception.AbstractException;

public abstract class AbstractUserException extends AbstractException {

    public AbstractUserException(String message) {
        super(message);
    }

}
