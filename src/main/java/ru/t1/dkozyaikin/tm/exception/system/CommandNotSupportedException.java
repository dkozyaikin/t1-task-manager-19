package ru.t1.dkozyaikin.tm.exception.system;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Argument is not supported...");
    }

    public CommandNotSupportedException(String command) {
        super("Error! Argument ''" + command + "'' is not supported...");
    }

}
