package ru.t1.dkozyaikin.tm.api.service;

import ru.t1.dkozyaikin.tm.enumerated.Status;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.model.Project;

public interface IProjectService extends IService<Project> {

    Project create(String name) throws AbstractException;

    Project create(String name, String description) throws AbstractException;

    Project create(String name, String description, Status status) throws AbstractException;

    Project updateById(String id, String name, String description) throws AbstractException;

    Project updateByIndex(Integer index, String name, String description) throws AbstractException;

    Project changeProjectStatusById(String id, Status status) throws AbstractException;

    Project changeProjectStatusByIndex(Integer index, Status status) throws AbstractException;

}
