package ru.t1.dkozyaikin.tm.api.service;

import ru.t1.dkozyaikin.tm.exception.AbstractException;

public interface IProjectTaskService {

    void bindTaskToProject(String projectId, String taskId);

    void removeProjectById(String projectId);

    void removeProjectByIndex(Integer projectIndex) throws AbstractException;

    void clearAllProjects();

    void unbindTaskFromProject(String projectId, String taskId);

}
