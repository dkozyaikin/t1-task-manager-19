package ru.t1.dkozyaikin.tm.api.repository;

import ru.t1.dkozyaikin.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    Task create(String name);

    Task create(String name, String description);

    List<Task> findAllTasksByProjectId(String projectId);

}

