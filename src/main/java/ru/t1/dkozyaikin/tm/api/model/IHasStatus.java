package ru.t1.dkozyaikin.tm.api.model;

import ru.t1.dkozyaikin.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
