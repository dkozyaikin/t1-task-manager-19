package ru.t1.dkozyaikin.tm.api.service;

import ru.t1.dkozyaikin.tm.enumerated.Status;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.model.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    Task create(String name) throws AbstractException;

    Task create(String name, String description) throws AbstractException;

    List<Task> findAllByProjectId(String projectId) throws AbstractException;

    Task updateById(String id, String name, String description) throws AbstractException;

    Task updateByIndex(Integer index, String name, String description) throws AbstractException;

    Task changeTaskStatusById(String id, Status status) throws AbstractException;

    Task changeTaskStatusByIndex(Integer index, Status status) throws AbstractException;

}
